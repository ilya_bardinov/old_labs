#include "pch.h"
#include "binary_tree.h"

using namespace std;


int main(int argc, char **argv)
{
    //std::cout << "1" << std::endl;
    BinaryTree *pTree = new BinaryTree();
    pTree->addVertex(0);
    pTree->addVertex(50);
    pTree->addVertex(100);
    pTree->addVertex(60);
    pTree->addVertex(70);
    pTree->addVertex(110);
    pTree->addVertex(120);
    pTree->addVertex(61);
    pTree->addVertex(62);
    pTree->addVertex(71);
    pTree->addVertex(72);
    pTree->addVertex(111);
    pTree->addVertex(112);
    pTree->addVertex(121);
    pTree->addVertex(122);
    pTree->print();
}
