#include "unsorted.h"

using namespace std;

int64 involution(int64 startNumber, int64 curNumber, int16 degree)
{
    if(startNumber == 0 && degree == 0)
        return 0;

    if(degree == 0)
        return curNumber;

    if(degree > 0)
        return involution(startNumber, curNumber*startNumber, degree-1);

    return involution(startNumber, curNumber/startNumber, degree+1);
}

int64 bitInvolution(int64 num, uint32 degree)
{
    int c = num;
    int res = 1;
    while(degree != 0) {
        if(degree && degree % 2 != 0) {
            res = res * c;
        }
        c *= c;
        degree >>= 1;
    }
    return res;
}

bool isPrime(uint64 number)
{
    if(number == 2 || number == 3)
        return true;

    if(number == 1 || number%2 == 0 || number%3 == 0)
        return false;

    uint64 k1 = 5, k2 = 7;
    for(uint64 i = 2; k1 <= sqrt(number); i++) {
        if(number%k1 == 0 || number%k2 == 0)
            return false;
        k1 = 6*i-1;
        k2 = 6*i+1;
    }
    return true;
}

uint64 greatestCommonDivisor(uint64 a, uint64 b)
{
    if(a == 0)
        return b;
    if(b == 0)
        return a;
    if(a == b)
        return a;
    if(a == 1)
        return 1;
    if(b == 1)
        return 1;
    if(a % 2 == 0 && b % 2 == 0)
        return 2 * greatestCommonDivisor(a / 2, b / 2);
    if(a % 2 == 0 && b % 2 != 0)
        return greatestCommonDivisor(a / 2, b);
    if(a % 2 != 0 && b % 2 == 0)
        return greatestCommonDivisor(a, b / 2);
    if(a % 2 != 0 && b % 2 != 0 && b > a)
        return greatestCommonDivisor((b - a) / 2, a);
    if(a % 2 != 0 && b % 2 != 0 && b < a)
        return greatestCommonDivisor((a - b) / 2, b);
    return 1;
}
