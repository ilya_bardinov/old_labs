#include "pch.h"

/* realization work with matrix */

class Vector
{
    public:

        Vector(const int line);
        Vector(const int line, const double *a);
        ~Vector();

        const Vector operator + (const Vector &a) const;
        const void operator += (const Vector &a);
        const Vector operator - (const Vector &a) const;
        const void operator -= (const Vector &a);
        const Vector operator * (const double a) const;
        const void operator *= (const double a);
        Vector & operator = (const Vector &a);
        
        const double operator [](const int j) const;

        void print() const;

    private:

        double *m_vector;
        int m_line;
};

class Matrix
{
    public:

        Matrix(const int line, const int column);
        Matrix(const int line, const int column, const double *a);
        ~Matrix();

        const Matrix operator + (const Matrix &a) const;
        const void operator += (const Matrix &a);
        const Matrix operator - (const Matrix &a) const;
        const void operator -= (const Matrix &a);
        const Matrix operator * (const double a) const;
        const void operator *= (const double a);
        const Matrix operator * (const Matrix &a) const;
        const void operator *= (const Matrix &a);
        Matrix & operator = (const Matrix &a);
        
        const Vector operator [](int i) const;

        Vector toVector() const;
        void print() const;

    private:

        double **m_matrix;
        int m_line;
        int m_column;
};
