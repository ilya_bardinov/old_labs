#include "pch.h"
#include "reverse_polish_notation.h"

int64 involution(int64 startNumber, int64 curNumber, int16 degree);
int64 bitInvolution(int64 num, uint32 degree);
bool isPrime(uint64 number);
uint64 greatestCommonDivisor(uint64 a, uint64 b);
