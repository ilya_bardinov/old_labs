#include "pch.h"

class TreeVertex
{
    public:

        TreeVertex(int key, TreeVertex *pParent, TreeVertex *pChildLeft, TreeVertex *pChildRight);
        ~TreeVertex();
        int value              = 0;
        TreeVertex *parent     = nullptr;
        TreeVertex *childLeft  = nullptr;
        TreeVertex *childRight = nullptr;
};

class BinaryTree
{
    public:

        ~BinaryTree();
        void addVertex(int value);
        void deleteVertex(int value);
        void clear();
        TreeVertex * find(int value);
        void print();

    private:

        std::vector <TreeVertex *> tree;
};
