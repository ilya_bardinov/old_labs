#include "reverse_polish_notation.h"
using namespace std;


void reversePolishNotation()
{
    cout << "Hello, this is a reverse polish notation. Welcome. Press ';' to end input." << endl;
    cout << "Enter an expression in reverse polish notation: ";
    Stack *stack = new Stack;
    string str;
    cin >> str;
    while(str != ";") {
        //cout << "you have entered string: " << str << endl;
        if((str[0] == '+' || str[0] == '*' || str[0] == '-' || str[0] == '/') && (str[1] == '\0' || str[1] == ';') && stack->getStackSize() >= 2) {
            int64 a = stack->top();
            stack->pop();
            int64 b = stack->top();
            stack->pop();
            switch(str[0]) {
                case '+':
                    stack->push(a + b);
                    cout << "stack put = " << a << " + " << b << " = " << a + b << endl;
                    break;
                case '-':
                    stack->push(b - a);
                    cout << "stack put = " << b << " - " << a << " = " << b - a << endl;
                    break;
                case '*':
                    stack->push(a * b);
                    cout << "stack put = " << a << " * " << b << " = " << a * b << endl;
                    break;
                case '/':
                    if(a == 0) {
                        cout << "Division by zero (0) - operation not stated - programm will be closed." << endl;
                        return;
                    }
                    stack->push(b / a);
                    cout << "stack put = " << b << " / " << a << " = " << b / a << endl;
                    break;
            }
            if(str[1] == ';')
                break;
            cin >> str;
            continue;
        }
        const char *cstr = str.c_str();
        char *endptr;
        int64 value = strtoll(cstr, &endptr, 10);
        if(*endptr != '\0') {
            cout << "Incorrect input: " << str << " => " << endptr << endl;
            cin >> str;
            continue;
        }
        stack->push(value);
        cout << "stack put = " << value << endl;
        cin >> str;
    }

    if(stack->getStackSize() != 1) {
        cout << "Error in stack: incorrect numbers of operands" << endl;
    } else {
        cout << "An expression's value is " << stack->top() << endl;
    }
    delete(stack);
}

void startStack(Stack * stack)
{
    std::cout << "=========================================================" << std::endl;
    std::cout << "1 - To add new element in stack end " << std::endl << "2 - To delete element from stack end";
    std::cout << std::endl << "3 - To get end's element's value" << std::endl << "4 - To get stack's size" << std::endl << "5 - To erase stack" << std::endl;
    std::cout << "9 - To see stack" << std::endl <<"99 - Go to programm start" << std::endl << " 0 - Exit" << std::endl;
    std::cout << "=========================================================" << std::endl;
    uint8 buffer;
    std::cin >> buffer;
    switch(buffer) {
        case 1: // new element
            std::cout << "Enter integer value: ";
            int64 value;
            std::cin >> value;
            stack->push(value);
            std::cout << "Number " << value << " was put in stack. " << std::endl;
            startStack(stack);
            break;
        case 2: // delete element
            std::cout << "Last element's value is " << stack->top() << std::endl;
            stack->pop();
            std::cout << "Last element was deleted" << std::endl;
            startStack(stack);
            break;
        case 3: // get last element
            std::cout << "Last element's value is " << stack->top() << std::endl;
            startStack(stack);
            break;
        case 4: // get stack size
            std::cout << "Stack size is " << stack->getStackSize() << std::endl;
            startStack(stack);
            break;
        case 5: // erase stack
            stack->clear();
            std::cout << "Stack was cleared" << std::endl;
            startStack(stack);
            break;
        case 9: // overview stack
            for(uint8 i = 0; i < stack->getStackSize(); i++) {
                std::cout << "Element [" << i << "] = " << stack->getValue(i) << std::endl;
            }
            startStack(stack);
            break;
        case 99: // start again
            startStack(stack);
            break;
        case 0: //exit
            stack->clear();
            break;
        default:
            std::cout << "Something goes wrong!"<< std::endl;
            startStack(stack);
            break;
    }
}

/*Stack::Stack() {
	std::cout << std::endl << "Stack was created successful" << std::endl;
}

Stack::~Stack() {
    std::cout << std::endl << "Stack was deleted successful" << std::endl;
}*/

void Stack::push(int64 elem)
{
    m_elem.push_back(elem);
}

void Stack::pop()
{
    if(!m_elem.empty()) {
        //int i = m_elem.size()-1;
        m_elem.erase(m_elem.end()-1);
    }
}

int64 Stack::top()
{
    if(!m_elem.empty())
        return m_elem.at(m_elem.size()-1);
    else {
        std::cout << std::endl << "==== Stack::back() return error ===" << std::endl;
        return 0;
    }

}

int64 Stack::getValue(uint8 i)
{
    if(!m_elem.empty() && m_elem.size() >= i + 1)
        return m_elem.at(i);
    else {
        std::cout << std::endl << "==== Stack::getValue(int i) return error ===" << std::endl;
        return 0;
    }
}

uint8 Stack::getStackSize()
{
    return m_elem.size();
}

void Stack::clear()
{
    m_elem.clear();
}
