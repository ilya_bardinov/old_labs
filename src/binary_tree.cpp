#include "binary_tree.h"

TreeVertex::TreeVertex(int key, TreeVertex *pParent, TreeVertex *pChildLeft, TreeVertex *pChildRight)
{
    value = key;
    parent = pParent;
    childLeft = pChildLeft;
    childRight = pChildRight;
}

TreeVertex::~TreeVertex()
{
    parent = nullptr;
    childLeft = nullptr;
    childRight = nullptr;
}

BinaryTree::~BinaryTree()
{
    this->clear();
}

void BinaryTree::addVertex(int value)
{
    if(tree.empty()) {
        TreeVertex *pVertex = new TreeVertex(value, nullptr, nullptr, nullptr);
        tree.push_back(pVertex);
        std::cout << "add element " << value << " as new" << std::endl;
    } else {
        for(int i = 0; i < tree.capacity()/2; i++) {
            if(tree[i+tree.capacity()/2-1])
                continue;
            else {
                TreeVertex *pVertex = new TreeVertex(value, tree[(i+tree.capacity()/2-2)/2], nullptr, nullptr);
                tree.push_back(pVertex);
                std::cout << "add element " << value << " as child of " << tree[(i+tree.capacity()/2-2)/2]->value << std::endl;
                return;
            }
        }
        TreeVertex *pVertex = new TreeVertex(value, tree[(tree.capacity()-1)/2], nullptr, nullptr);
        tree.push_back(pVertex);
        std::cout << "add element " << value << " to new line and child of " << tree[(tree.capacity()-1)/2]->value << " and capacity " << tree.capacity()*2 << std::endl;
        tree.reserve(tree.capacity()*2);
    }
}

void BinaryTree::deleteVertex(int value)
{
    if(tree.empty()) {
        std::cout << "Binary tree is empty" << std::endl;
        return;
    }
    for(auto it = tree.begin(); it != tree.end(); it++) {
        if((*it)->value == value && ((*it)->childLeft == nullptr || (*it)->childRight == nullptr)) {
            // ATTN: don't delete element if two children! only for 0 or 1 children!
            if((*it)->childLeft != nullptr)
                (*it)->parent = (*it)->childLeft;
            else
                (*it)->parent = (*it)->childRight;
            tree.erase(it);
            delete(*it);
            break;
        }
    }
}

void BinaryTree::clear()
{
    for(auto it = tree.begin(); it != tree.end(); ++it)
        delete(*it);
    tree.clear();
    tree.shrink_to_fit();
}

TreeVertex * BinaryTree::find(int value)
{
    if(tree.empty()) {
        std::cout << "Binary tree is empty" << std::endl;
        return nullptr;
    }
    for(auto it = tree.begin(); it != tree.end(); it++) {
        if((*it)->value == value)
            return (*it);
    }
    return nullptr;
}

void BinaryTree::print()
{
    for(int i = tree.capacity()/2 - 1; i < tree.capacity()/2 - 1 + tree.capacity()/2; i++) {
        TreeVertex *pVertex = tree[i];
        while(pVertex) {
            std::cout << pVertex->value << " <= ";
            pVertex = pVertex->parent;
        }
        std::cout << "begin" << std::endl;
    }
}
