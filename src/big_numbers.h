#include "pch.h"

/* realization work with very long integer numbers */

void initCalculatingSystem(); // main body for parsing and call calculating
std::string calculateSum(std::string strFirstOperand, std::string strSecondOperand); // sum
std::string calculateDiff(std::string strFirstOperand, std::string strSecondOperand, char *outputSign); // difference
std::string calculateMultiplication(std::string str1, std::string str2); // multiplication
std::string calculateDivision(std::string str1, std::string str2); // division without residue
